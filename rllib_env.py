from gym import spaces
import numpy as np
import itertools as it
import h5py
from ray.rllib.env.multi_agent_env import MultiAgentEnv


class env(MultiAgentEnv):
    def __init__(self, config):
        #super(env, self).__init__()
        self.band_width = 180e3
        self.num_BS=4 # number of base stations
        self.mu=4 # inefficiency of the power amplifier of the users
        self.data = h5py.File(config['filename'],'r')
        self.channel_data = self.data.get('training').get('input')
        self.channel = np.array(self.channel_data[np.random.randint(0,np.array(self.channel_data).shape[0])]) 
        self.state = self.channel[:-1] # initial state of the env
        self.num_UE= int(np.sqrt(self.state.shape)) # number of users

        self.observation_space = spaces.Box(-np.Inf,np.Inf,shape=(self.num_UE*self.num_UE + self.num_UE+1,),dtype=np.float32) # observation space of the environment
        self.action_space = spaces.Box(low=0,high=1,shape=(1,),dtype=np.float32) # action space of the environment

    #agent takes an action in an episode
    def step(self, action_dict):
        
        self.pC = 1 # circuit power consumption
        self.p_max= 10**self.channel[-1] # max power allocation 
        self.gain = np.reshape(10**self.state,(self.num_UE,self.num_UE)) # channel gains in linear scale
        self.alpha= np.diag(self.gain) # channel realization alpha
        self.beta = self.gain - np.eye(self.gain.shape[0])*self.alpha # channel realization beta
        action = []
        obs,rew,done,info = {}, {}, {}, {} #dictionary for multiagent RL


        for key in action_dict:
            action = np.append(action, action_dict[key])


        #calculation of reward (Energy Efficiency)
        deno = 1+(np.sum(self.beta*action,axis=1))
        num = np.multiply(self.alpha,action)
        frac = num / deno
        rate=np.log2(1+frac)

        power=self.mu*self.p_max*action + self.pC

        reward = self.band_width*1e-6*np.sum(rate/power)

        for i in range(self.num_UE):
            hot = np.zeros(self.num_UE)
            hot[i] = 1
            obs[i], rew[i], done[i], info[i] = np.hstack((self.channel,hot)), reward, True, {}
        done['__all__'] = True



        return obs, rew, done, info

    #reset the environment to a new state after the end of episode
    def reset(self):
        i=np.random.randint(0,np.array(self.channel_data).shape[0])
        self.channel = np.array(self.permute(self.channel_data[i]))
        self.state = self.channel[:-1]
        obs = {}

        for i in range(self.num_UE):
            hot = np.zeros(self.num_UE)
            hot[i] = 1
            obs[i] = np.hstack((self.channel,hot))
        return obs

    def permute(self,x):
        permutations = list(it.permutations(range(self.num_UE)))
        permutations = np.array(permutations)
        num_perms, num_ue = np.shape(permutations)
        _perm = np.random.randint(0,num_perms,(1,))
        perm = permutations[_perm[0],:]
        t = np.tile(perm,num_ue)
        r = np.repeat(perm,num_ue,axis=0)
        perm_idx = num_ue*r + t
        self.perm_idx = np.concatenate((perm_idx,[num_ue**2]))
        p = np.take(x,self.perm_idx,axis=-1)
        return p
