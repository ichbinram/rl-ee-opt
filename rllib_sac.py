import ray
from ray import tune

from rllib_env import env
import ray.rllib.agents.sac as sac
import Multi_RlHelper
import matplotlib.pyplot as plt
import numpy as np

ray.shutdown()
ray.init(ignore_reinit_error=True)

filename = 'dataset/dset4.h5'
chkpt_root = 'chkpt'

config = sac.DEFAULT_CONFIG.copy()
config["env"] = env
config["env_config"] = {'filename':filename,'training':True}
config["Q_model"]["fcnet_hidden"] = [256,256] 
config["Q_model"]["fcnet_activation"] = 'relu' 
config["policy_model"]["fcnet_hidden"] = [256,256] 
config["policy_model"]["fcnet_activation"] = 'relu' 
config["timesteps_per_iteration"] = 1
config["framework"] = "torch"
config["optimization"]["actor_learning_rate"] = 1e-6
config["optimization"]["entropy_learning_rate"] = 1e-3
config["train_batch_size"] = 2048
config["normalize_actions"] = True
config["learning_starts"] = 1000
config["tau"] = 0.0115
config["clip_actions"] = True
config["horizon"] = 1
config["target_entropy"] = 'auto'
config["explore"] = True
config["num_workers"] = 8

#Training using SACTrainer
results = tune.run(sac.SACTrainer, config=config, local_dir = chkpt_root, name = "SAC_rllib", stop={'timesteps_total':500000}, checkpoint_at_end= True, verbose=0)

# The helper class doesn't work well when loading the checkpoint. This is because of how rllib stores the checkpoints.

'''
stop = {'timesteps_total':500000}
log_dir = './trials'
filename = 'dataset/dset4.h5'
trainer = Multi_RlHelper.RlHelper(config = config, save_dir = log_dir)
checkpoint_path, analysis = trainer.train(stop_criteria = stop)
trainer.load(checkpoint_path)
reward, p_max = trainer.test(filename)

#plotting against bb
plot_y = np.reshape(reward,(int(len(reward)/51),51))
plot_y = np.mean(plot_y,axis=0)

plt.plot(p_max,np.transpose(plot_y),label = 'SAC')
plt.ylabel('Mbits/J')
plt.xlabel('dBW')
plt.legend()
plt.grid(True)
plt.savefig('multiagent_bb.png')
'''
