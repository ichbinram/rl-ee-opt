import h5py
import env
from stable_baselines3 import SAC
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import argparse

def create_model(filename, checkpoint):

    chan= env.env(filename)
# create a model of SAC using MlpPolicy
    if checkpoint == None:
        model = SAC("MlpPolicy", chan, learning_rate=0.001, batch_size=2048, ent_coef = 'auto', tensorboard_log= 'logs',verbose=1, tau=0.001)
    else: 
        model = SAC.load(checkpoint, chan)

    return model 

def main(chan, model, dataset, savefile, mode:int):
    if mode == 1:

    # learning phase
        model.learn(total_timesteps=500000, log_interval=250)
        model.save(savefile)

    elif mode == 2:
        # testing phase 
        test_model = SAC.load(savefile)
        p_max,ee = test(test_model,dataset,chan)
        plot(p_max,ee)
    
    else:
    # learning phase
        model.learn(total_timesteps=500000, log_interval=250)
        model.save(savefile)
    
    # testing phase
        test_model = SAC.load(savefile)
        p_max,ee = test(test_model,dataset,chan)
        plot(p_max,ee)
    return

def test(model, dataset , env):
    data = h5py.File(dataset, 'r')
    input_data = data.get('test')
    channel = np.array(input_data.get('input'))
    p_max = channel[:,-1]
    ee = []

    for i in range(channel.shape[0]):
        obs = channel[i][:-1]
        action,_ = model.predict(obs,deterministic=True)
        env.channel = channel[i]
        env.state = env.channel[:-1]
        reward = env.step(action)[1]
        ee.append(reward)

    return p_max, np.array(ee)

def plot(p_max,ee):
    mean_ee = np.reshape(ee, (int(ee.shape[0]/51),51))
    mean_ee = np.mean(mean_ee, axis=0)
    result = {'p_max': 10*p_max[:51], 'ee': mean_ee}
    pd.DataFrame.from_dict(result).to_csv('result_single_sac.dat', index=False, sep='\t')

    plt.plot(p_max[:51],np.transpose(mean_ee))
    plt.ylabel('Energy efficiency (Mbits/J)')
    plt.xlabel('Max power (dBW)')
    plt.grid(True)
    plt.show()

    return



if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='apply sac on a dataset')
    parser.add_argument("-f", "--dataset", type=str, help="location of the dataset")
    parser.add_argument("-s", "--save", type=str, default="sac_model")
    parser.add_argument("-l", "--load", type=str, default=None, help="location of model to load")
    parser.add_argument("-m", "--mode", type=int, default=0, help="1=training, 2=testing or 0=complete run")
    arg = parser.parse_args()
    model = create_model(arg.dataset, arg.load)
    channel = env.env(arg.dataset)
    main(channel,model,arg.dataset,arg.save, arg.mode)

