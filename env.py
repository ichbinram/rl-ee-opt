import gym 
from gym import spaces
import numpy as np
import itertools as it
import h5py


class env(gym.Env):
    def __init__(self,filename):
        super(env, self).__init__()
        self.band_width = 180e3
        self.n_R=2 # num of antenna
        self.num_BS=4 # number of base stations
        self.mu=4 # inefficiency of the power amplifier of the users
        self.data = h5py.File(filename,'r')
        self.channel_data = self.data.get('training').get('input')
        self.channel = np.array(self.channel_data[np.random.randint(0,np.array(self.channel_data).shape[0])]) # random initialization
        self.state = self.channel[:-1] # channel gains of the state
        self.num_UE=int(np.sqrt(self.state.shape)) # number of users

        self.observation_space = spaces.Box(-np.Inf,np.Inf,shape=(self.num_UE*self.num_UE + 1,),dtype=np.float32) # observation space of the environment
        self.action_space = spaces.Box(low=0,high=1,shape=(self.num_UE,),dtype=np.float32) # action space of the environment

    #agent takes an action in an episode
    def step(self, action):
        
        self.pC = 1 # circuit power consumption
        self.p_max= 10**self.channel[-1] # max power allocation 
        self.gain = np.reshape(10**self.state,(self.num_UE,self.num_UE))
        self.alpha= np.diag(self.gain) # channel realization alpha
        self.beta = self.gain - np.eye(self.gain.shape[0])*self.alpha # channel realization beta


        #calculation of reward (Energy Efficiency)
        deno = 1+(np.sum(self.beta*action,axis=1))
        num = np.multiply(self.alpha,action)
        frac = num / deno
        rate=np.log2(1+frac)

        power=self.mu*self.p_max*action + self.pC

        reward = self.band_width*1e-6*np.sum(rate/power)

        done = True
        obs = np.array(self.channel)

        return obs, float(reward), done,{}

    #reset the environment to a new state after the end of episode
    def reset(self):
        i=np.random.randint(0,np.array(self.channel_data).shape[0])
        self.channel = np.array(self.permute(self.channel_data[i]))
        self.state = self.channel[:-1]
        obs = np.array(self.channel)
        return obs

    def permute(self,x):
        permutations = list(it.permutations(range(self.num_UE)))
        permutations = np.array(permutations)
        num_perms, num_ue = np.shape(permutations)
        _perm = np.random.randint(0,num_perms)
        #p = []
        #for _perm in range(num_perms-23):
        perm = permutations[_perm,:]
        t = np.tile(perm,num_ue)
        r = np.repeat(perm,num_ue,axis=0)
        perm_idx = num_ue*r + t
        self.perm_idx = np.concatenate((perm_idx,[num_ue**2]))
        p = np.take(x,self.perm_idx,axis=-1)
        #p = np.array(p)
        #p = p.reshape(p.shape[0]*p.shape[1],p.shape[-1])
        return p
