import ray
import numpy as np
import pandas as pd

from rllib_env import env
from env import env as __env
import ray.rllib.agents.sac as sac
import h5py
import matplotlib.pyplot as plt
import Multi_RlHelper

checkpoint_path = 'chkpt/SAC_rllib/SAC_env_1f861_00000_0_2021-11-12_00-14-30/checkpoint_026760/checkpoint-26760'

filename='dataset/dset7.h5' #use the location of dataset
data=h5py.File(filename,'r')
input_data = data.get('test')
channel = np.array(input_data.get('input'))
power_opt = np.array(input_data.get('xopt'))

ray.init(ignore_reinit_error=True)


#Preparation of Config dictionary
config = sac.DEFAULT_CONFIG.copy()
config["env"] = env
config["env_config"] = {'filename':filename}
config["framework"] = "torch"
config["optimization"]["actor_learning_rate"] = 1e-5
config["train_batch_size"] = 2048
config["monitor"] = True
config["normalize_actions"] = True
config["learning_starts"] = 1000
config["tau"] = 0.001
config["clip_actions"] = True
config["horizon"] = 1
config["explore"] = False
config["target_entropy"] = 'auto'

log_dir = './trials'
trainer = Multi_RlHelper.RlHelper(config = config, save_dir = log_dir)
trainer.load(checkpoint_path)
reward,p_max = trainer.test(filename)
ee_act = np.zeros(channel.shape[0])
i=0
_env=__env(filename)

while (i<channel.shape[0]):
    action_real = 10**power_opt[i]
    _env.channel = channel[i]
    _env.state = _env.channel[:-1]
    ee_act[i] = _env.step(action=action_real)[1]
    i+=1

plot = np.reshape(reward, (int(len(reward)/51),51))
plot = np.nanmean(plot, axis=0)

results = {'pmax': p_max, 'reward': plot}
pd.DataFrame.from_dict(results).to_csv('multi_result_sac_permute_1o5m.dat', index = False, sep='\t') #save results to file
plot_real = np.reshape(ee_act, (int(ee_act.shape[0]/51),51))
plot_real = np.nanmean(plot_real, axis=0)

plt.plot(p_max,np.transpose(plot), label='MultiSAC')
plt.plot(p_max,np.transpose(plot_real), label='BB')
plt.ylabel('reward (Mbits/J)')
plt.xlabel('p_max (dBW)')
plt.legend()
plt.grid(True)
plt.show()

