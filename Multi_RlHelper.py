import ray
import ray.rllib.agents.sac as sac
import h5py
import numpy as np
from env import env as _env

class RlHelper:
    def __init__(self, config, save_dir):
        self.config = config
        self.env_class = config['env']
        self.env_config = config['env_config']
        self.save_dir = save_dir
        self.agent = None
		
    def train(self, stop_criteria):

        analysis = ray.tune.run(sac.SACTrainer, config=self.config, local_dir=self.save_dir, stop=stop_criteria,
								checkpoint_at_end=True)
		# list of lists: one list per checkpoint; each checkpoint list contains 1st the path, 2nd the metric value
        checkpoints = analysis.best_trial.get_trial_checkpoints_paths(analysis.get_best_trial('episode_reward_mean',mode='max'), metric='episode_reward_mean')
		# retrieve the checkpoint path; we only have a single checkpoint, so take the first one
        checkpoint_path = checkpoints[0][0]
        return checkpoint_path, analysis
	
    def load(self, path):
        """
		Load a trained RLlib agent from the specified path. Call this before testing a trained agent.
		:param path: Path pointing to the agent's saved checkpoint (only used for RLlib agents)
		"""
        self.agent = sac.SACTrainer(config=self.config, env=self.env_class)
        self.agent.restore(path)

    def test(self, filename):
        """Test trained agent for a single episode. Return the episode reward""" 
        assert self.agent is not None, "Load agent before testing"
        data = h5py.File(filename,'r')
        input_data = data.get('test')
        channel = np.array(input_data.get('input'))
		
		# instantiate env class
        env = _env(filename)
		# run until episode ends
        p_max = channel[:51,-1] 
        reward = [] 
        obs = {}
        action = np.zeros(env.num_UE)

        for i in range(channel.shape[0]):
            env.channel = channel[i]
            env.state = env.channel[:-1]
            for pos in range(env.num_UE):
                hot = np.zeros(env.num_UE)
                hot[pos] = 1
                obs[pos] = np.hstack((np.array(channel[i]), hot))

            for _id, _obs in obs.items():
                action[_id] = self.agent.compute_action(_obs)
            print(action)
            _obs, rew, done, info = env.step(action)
            reward.append(rew)       

        return reward, p_max
