# Reinforcement Learning-Based Global Programming for Energy Efficiency in Multi-Cell Interference Networks
This code is related to the following article:
Ramprasad Raghunath, Bile Peng, Karl-Ludwig Besser, and Eduard A. Jorswieck "Reinforcement Learning-Based Global Programming for Energy Efficiency in Multi-Cell Interference Networks"
```
DOI: 10.1109/ICC45855.2022.9838408
```

## Abstract
With the increasing application of internet of things (IoT), the number of wirelessly transmitting devices is on a rise.
It is important that the Energy efficiency (EE) is maximized to reduce interference and save power. This work explores the possibility of power control for maximum EE in wireless interference networks using techniques of Reinforcement learning (RL). The RL algorithm used is soft-actor critic (SAC) based on entropy regularization that allows to escape the local optima and encourages exploration.
This enables solving the energy efficient power control problem with reduced complexity. We demonstrate that this approach is able to obtain solutions close to the global optimum. In contrast to supervised \gls{ml} techniques, this can be done without the need of any kind of labeled data in the training phase. The model free approach and the unsupervised nature of \gls{rl} therefore reduce the required computational effort.

## Requirements
All the required packages for single and multi-agent environments are mentioned in the files requirement.txt and requirement_rllib.txt.
Package installation for single agent environment:
1. pip install stable-baselines3[extra]
2. pip install gym

and for multi-agent environment:
1. pip install ray
2. pip install 'ray[rllib]'


Now common packages required for both code:
1. pip install h5py
2. pip install numpy
3. pip install torch
4. pip install pandas

## Citation information
Please use the following bibtex entry to cite our work.
``` 
@inproceedings{Raghunath2022icc,
	author = {Raghunath, Ramprasad and Peng, Bile and Besser, Karl-Ludwig 
	and Jorswieck, Eduard A.},
	title = {Reinforcement Learning-Based Global Programming for Energy 
	Efficiency in Multi-Cell Interference Networks},
	booktitle = {ICC 2022 -- IEEE International Conference on Communications},
	year = {2022},
	month = {5},
	pages = {5499--5504},
	publisher = {IEEE},
	venue = {Seoul, South Korea},
	doi = {10.1109/ICC45855.2022.9838408},
} 
```
