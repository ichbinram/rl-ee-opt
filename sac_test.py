import env as chan
from stable_baselines3 import SAC
import numpy as np
import h5py
import matplotlib.pyplot as plt
import pandas as pd

filename='dataset/dset4.h5'
data=h5py.File(filename,'r')
input_data=data.get('test')
channel = np.array(input_data.get('input'))
print(channel.shape)
power=np.array(input_data.get('xopt'))
ee=np.array(input_data.get('objval'))
model=SAC.load('models/sac_model.zip')
env=chan.env(filename)
i=0
p_max = channel[:,-1]
rew = []
act = []

while (i < channel.shape[0]):
    obs = channel[i][:-1]
    action,_ = model.predict(obs,deterministic=True)
    action_real = 10**power[i]
    env.channel = channel[i]

    env.state = env.channel[:-1]
    reward = env.step(action)[1]
    ee_act = env.step(action_real)[1]
    rew.append(reward)
    act.append(ee_act)
    print('i= ',i)
    i+=1

plot_y = np.reshape(rew, (int(len(rew)/51),51))
plot_y = np.mean(plot_y, axis=0) 
plot_x = np.reshape(act, (int(len(act)/51),51))
plot_x = np.mean(plot_x, axis=0) 

result ={"pmax": 10*p_max[:51], "reward": plot_y}

pd.DataFrame.from_dict(result).to_csv("result_7usr.dat",index=False, sep="\t") #save results to data file

plt.plot(p_max[:51],np.transpose(plot_y),label='SAC')
plt.plot(p_max[:51],np.transpose(plot_x),label='BB')
plt.ylabel('reward (Mbits/J)')
plt.xlabel('p_max (dBW)')
plt.legend()
plt.grid(True)
plt.show()

